package org.app.service.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Departament {
	@Id
	private Integer idDepartament;
	
	private String denumireDepartament;
	
	@OneToMany
	private List<Post> posturi = new ArrayList<Post>();

	public Integer getIdDepartament() {
		return idDepartament;
	}

	public void setIdDepartament(Integer idDepartament) {
		this.idDepartament = idDepartament;
	}

	public String getDenumireDepartament() {
		return denumireDepartament;
	}

	public void setDenumireDepartament(String denumireDepartament) {
		this.denumireDepartament = denumireDepartament;
	}

	public List<Post> getPosturi() {
		return posturi;
	}

	public void setPosturi(List<Post> posturi) {
		this.posturi = posturi;
	}

	public Departament(Integer idDepartament, String denumireDepartament) {
		super();
		this.idDepartament = idDepartament;
		this.denumireDepartament = denumireDepartament;
	}

	public Departament() {
		super();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idDepartament == null) ? 0 : idDepartament.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Departament other = (Departament) obj;
		if (idDepartament == null) {
			if (other.idDepartament != null)
				return false;
		} else if (!idDepartament.equals(other.idDepartament))
			return false;
		return true;
	}
	
}
