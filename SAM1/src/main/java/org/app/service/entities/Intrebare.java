package org.app.service.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Intrebare {
	@Id
	private Integer idIntrebare;
	
	private String tipIntrebare;
	private String descriere;
	
	@ManyToOne
	private Test t;

	public Integer getIdIntrebare() {
		return idIntrebare;
	}

	public void setIdIntrebare(Integer idIntrebare) {
		this.idIntrebare = idIntrebare;
	}

	public String getTipIntrebare() {
		return tipIntrebare;
	}

	public void setTipIntrebare(String tipIntrebare) {
		this.tipIntrebare = tipIntrebare;
	}

	public String getDescriere() {
		return descriere;
	}

	public void setDescriere(String descriere) {
		this.descriere = descriere;
	}

	public Test getT() {
		return t;
	}

	public void setT(Test t) {
		this.t = t;
	}

	public Intrebare(Integer idIntrebare, String tipIntrebare, String descriere, Test t) {
		super();
		this.idIntrebare = idIntrebare;
		this.tipIntrebare = tipIntrebare;
		this.descriere = descriere;
		this.t = t;
	}

	public Intrebare() {
		super();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idIntrebare == null) ? 0 : idIntrebare.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Intrebare other = (Intrebare) obj;
		if (idIntrebare == null) {
			if (other.idIntrebare != null)
				return false;
		} else if (!idIntrebare.equals(other.idIntrebare))
			return false;
		return true;
	}	
}
