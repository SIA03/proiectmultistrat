package org.app.service.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Post {
	@Id
	private Integer idPost;
	
	private String denumirePost;
	private String descrierePost;
	
	@ManyToOne
	private Candidat c;
	
	@ManyToOne 
	private Departament d;

	public Integer getIdPost() {
		return idPost;
	}

	public void setIdPost(Integer idPost) {
		this.idPost = idPost;
	}

	public String getDenumirePost() {
		return denumirePost;
	}

	public void setDenumirePost(String denumirePost) {
		this.denumirePost = denumirePost;
	}

	public String getDescrierePost() {
		return descrierePost;
	}

	public void setDescrierePost(String descrierePost) {
		this.descrierePost = descrierePost;
	}

	public Candidat getC() {
		return c;
	}

	public void setC(Candidat c) {
		this.c = c;
	}

	public Departament getD() {
		return d;
	}

	public void setD(Departament d) {
		this.d = d;
	}

	public Post(Integer idPost, String denumirePost, String descrierePost, Candidat c, Departament d) {
		super();
		this.idPost = idPost;
		this.denumirePost = denumirePost;
		this.descrierePost = descrierePost;
		this.c = c;
		this.d = d;
	}

	public Post() {
		super();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idPost == null) ? 0 : idPost.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Post other = (Post) obj;
		if (idPost == null) {
			if (other.idPost != null)
				return false;
		} else if (!idPost.equals(other.idPost))
			return false;
		return true;
	}

}
