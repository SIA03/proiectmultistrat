package org.app.service.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Test {
	@Id
	private Integer idTest;
	private String tipTest;
	
	@ManyToOne
	private Candidat c;
	
	@OneToMany
	private List<Intrebare> intrebari = new ArrayList<Intrebare>();

	public Integer getIdTest() {
		return idTest;
	}

	public void setIdTest(Integer idTest) {
		this.idTest = idTest;
	}

	public String getTipTest() {
		return tipTest;
	}

	public void setTipTest(String tipTest) {
		this.tipTest = tipTest;
	}

	public Candidat getC() {
		return c;
	}

	public void setC(Candidat c) {
		this.c = c;
	}

	public List<Intrebare> getIntrebari() {
		return intrebari;
	}

	public void setIntrebari(List<Intrebare> intrebari) {
		this.intrebari = intrebari;
	}

	public Test(Integer idTest, String tipTest, Candidat c) {
		super();
		this.idTest = idTest;
		this.tipTest = tipTest;
		this.c = c;
	}

	public Test() {
		super();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idTest == null) ? 0 : idTest.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Test other = (Test) obj;
		if (idTest == null) {
			if (other.idTest != null)
				return false;
		} else if (!idTest.equals(other.idTest))
			return false;
		return true;
	}
}
