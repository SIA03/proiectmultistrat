package org.app.service.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Candidat {
	@Id
	private Integer idCandidat;
	
	private String numeCandidat;
	private String email;
	private String telefon;
	
	@OneToMany
	private List<Test> teste = new ArrayList<Test>();
	
	@OneToMany
	private List<Interviu> interviuri = new ArrayList<Interviu>();
	
	@OneToMany
	private List<Post> posturi = new ArrayList<Post>();

	public Integer getIdCandidat() {
		return idCandidat;
	}

	public void setIdCandidat(Integer idCandidat) {
		this.idCandidat = idCandidat;
	}

	public String getNumeCandidat() {
		return numeCandidat;
	}

	public void setNumeCandidat(String numeCandidat) {
		this.numeCandidat = numeCandidat;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public List<Test> getTeste() {
		return teste;
	}

	public void setTeste(List<Test> teste) {
		this.teste = teste;
	}

	public List<Interviu> getInterviuri() {
		return interviuri;
	}

	public void setInterviuri(List<Interviu> interviuri) {
		this.interviuri = interviuri;
	}

	public List<Post> getPosturi() {
		return posturi;
	}

	public void setPosturi(List<Post> posturi) {
		this.posturi = posturi;
	}

	public Candidat(Integer idCandidat, String numeCandidat, String email, String telefon) {
		super();
		this.idCandidat = idCandidat;
		this.numeCandidat = numeCandidat;
		this.email = email;
		this.telefon = telefon;
	}

	public Candidat() {
		super();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idCandidat == null) ? 0 : idCandidat.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Candidat other = (Candidat) obj;
		if (idCandidat == null) {
			if (other.idCandidat != null)
				return false;
		} else if (!idCandidat.equals(other.idCandidat))
			return false;
		return true;
	}
	
	
}
