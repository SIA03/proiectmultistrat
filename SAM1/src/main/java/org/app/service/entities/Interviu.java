package org.app.service.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
@Entity
public class Interviu {
	@Id
	private Integer idInterviu;
	
	private Date data;
	
	private String status;
	
	@ManyToOne
	private Candidat c;

	public Integer getIdInterviu() {
		return idInterviu;
	}

	public void setIdInterviu(Integer idInterviu) {
		this.idInterviu = idInterviu;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Candidat getC() {
		return c;
	}

	public void setC(Candidat c) {
		this.c = c;
	}

	public Interviu(Integer idInterviu, Date data, String status, Candidat c) {
		super();
		this.idInterviu = idInterviu;
		this.data = data;
		this.status = status;
		this.c = c;
	}

	public Interviu() {
		super();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idInterviu == null) ? 0 : idInterviu.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Interviu other = (Interviu) obj;
		if (idInterviu == null) {
			if (other.idInterviu != null)
				return false;
		} else if (!idInterviu.equals(other.idInterviu))
			return false;
		return true;
	}		
}
